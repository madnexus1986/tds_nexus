// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS_Nexus/Weapons/Projectiles/ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TPS_NEXUS_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplode(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit) override;

	virtual void ImpactProjectile() override;

	void Explode();

	UFUNCTION(NetMulticast, Reliable)
		void SpawnExplodeFX_Multicast(UParticleSystem* FXTemplate);
	UFUNCTION(NetMulticast, Reliable)
		void SpawnExplodeSoundFX_Multicast(USoundBase* HitSound);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
	bool TimerEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
	float TimerToExplode = 0.0f;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
	//float TimeToExplode = 5.0f;
};
