// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_StateEffect.h"
#include "TPS_Nexus/Character/TPS_HealthComponent.h"
#include "TPS_Nexus/Interface/TPS_IGameActor.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"


bool UTPS_StateEffect::InitObject(AActor* Actor, FName BoneNameHitted)
{
	myActor = Actor;
	BoneNameToAttach = BoneNameHitted;
	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}
	thisStateEffectObject = this;

	return true;
}

void UTPS_StateEffect::ExecuteEffect(float DeltaTime)
{
}

void UTPS_StateEffect::DestroyObject()
{
	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
		myInterface->Execute_SetEffectRemoveForClient(myActor,thisStateEffectObject);
	}

	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}



bool UTPS_StateEffect::CheckStackableEffect()
{
	return false;
}

bool UTPS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName BoneNameHitted)
{
	Super::InitObject(Actor, BoneNameHitted);
	ExecuteOnce();
	return true;
}


void UTPS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();

}

//TArray<UParticleSystemComponent*> UTPS_StateEffect::GetAllParticleEmmiters()
//{
//	return TArray<UParticleSystemComponent*>();
//}

void UTPS_StateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTPS_StateEffect, BoneNameToAttach);
	DOREPLIFETIME(UTPS_StateEffect, EffectOffset);
	//DOREPLIFETIME(UTPS_StateEffect, ParticleEmitter);

}

void UTPS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTPS_HealthComponent* myHealthComp = Cast<UTPS_HealthComponent>(myActor->GetComponentByClass(UTPS_HealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		}
		ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
		if (ParticleEffect && myInterface && !bIsAutoDestroyParticleEffect)
		{
			myInterface->GetParticleStateEffectComponent(ParticleEffect, NAME_None);
			myInterface->Execute_SetEffectAddForClient(myActor, thisStateEffectObject);
		}
	}
	DestroyObject();
}

bool UTPS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName BoneNameHitted)
{
	Super::InitObject(Actor, BoneNameHitted);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTPS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTPS_StateEffect_ExecuteTimer::Execute, RateTime, true);
	}

	//remove all if tutorial is right
	//if (ParticleEffect.Num() > 0)
	//{
	//	FXSpawnByStateEffect_Multicast(Actor, BoneNameHitted);
	//}
	//if (ParticleEffect.Num() > 0)
	if (ParticleEffect)
	{
			ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
			if (myInterface)
			{
				//if (ApplyRandomParticleEffect)
				//{
				//	ParticleEmitter.Add(myInterface->GetParticleStateEffectComponent(ParticleEffect[rand() % ParticleEffect.Num()], BoneNameHitted));
				//}
				//else
				//{
				//	for (int8 i = 0; i < ParticleEffect.Num(); i++)
				//	{
				//		ParticleEmitter.Add(myInterface->GetParticleStateEffectComponent(ParticleEffect[i], BoneNameHitted));
				//	}
				//}

					myInterface->GetParticleStateEffectComponent(ParticleEffect, BoneNameHitted);
					myInterface->Execute_SetEffectAddForClient(Actor, thisStateEffectObject);

			}
			
			//think about adding effects even if there is no interface on actor/ just spawn it on rootcomponent
	}
	

	return true;
}

void UTPS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}

	//RemoveEffect_Multicast();
	//for (int8 i = 0; i < ParticleEmitter.Num(); i++)
	//{
	//	//remove if multiplayer applied
	//	ParticleEmitter[i]->DeactivateSystem();
	//	ParticleEmitter[i]->DestroyComponent();
	//	ParticleEmitter[i].RemoveAt(i);
	//	//ParticleEmitter = nullptr;
	//}
	//maybe after that worked on strver just call multicast remove?
	//RemoveEffect_Multicast();
	Super::DestroyObject();

}

void UTPS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTPS_HealthComponent* myHealthComp = Cast<UTPS_HealthComponent>(myActor->GetComponentByClass(UTPS_HealthComponent::StaticClass()));
		if (myHealthComp)
		{
				myHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}

}

//void UTPS_StateEffect_ExecuteTimer::FXSpawnByStateEffect_Multicast_Implementation(AActor* Actor, FName BoneNameHitted)
//{
//	//remove if everything right in tutorial
//	if (ParticleEffect.Num() > 0)
//	{
//		ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
//		if (myInterface)
//		{
//			if (ApplyRandomParticleEffect)
//			{
//				ParticleEmitter.Add(myInterface->GetParticleStateEffectComponent(ParticleEffect[rand() % ParticleEffect.Num()], BoneNameHitted));
//			}
//			else
//			{
//				for (int8 i = 0; i < ParticleEffect.Num(); i++)
//				{
//					ParticleEmitter.Add(myInterface->GetParticleStateEffectComponent(ParticleEffect[i], BoneNameHitted));
//				}
//			}
//
//		}
//		//think about adding effects even if there is no interface on actor/ just spawn it on rootcomponent
//	}
//}



//void UTPS_StateEffect_ExecuteTimer::RemoveEffect_Multicast_Implementation()
//{
//	//for (int8 i = 0; i < ParticleEmitter.Num(); i++)
//	//{
//	//	//remove if multiplayer applied
//	//	ParticleEmitter[i]->DeactivateSystem();
//	//	ParticleEmitter[i]->DestroyComponent();
//	//	ParticleEmitter[i].RemoveAt(i);
//	//	//ParticleEmitter = nullptr;
//
//
//	//}
//}

//TArray<UParticleSystemComponent*> UTPS_StateEffect_ExecuteTimer::GetAllParticleEmmiters()
//{
//	return ParticleEmitter;
//}
