// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "TPS_StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TPS_NEXUS_API UTPS_StateEffect : public UObject
{
	GENERATED_BODY()
	
public:
	bool IsSupportedForNetworking()const override { return true;};
	virtual bool InitObject(AActor* Actor, FName BoneNameHitted);
	virtual void ExecuteEffect(float DeltaTime);
	virtual void DestroyObject();

	//virtual TArray<UParticleSystemComponent*> GetAllParticleEmmiters();
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual bool CheckStackableEffect();

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStakable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		bool ApplyRandomParticleEffect = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		bool bIsAutoDestroyParticleEffect = false;

	UTPS_StateEffect* thisStateEffectObject = nullptr;
	AActor* myActor = nullptr;
	UPROPERTY(Replicated)
	FVector EffectOffset = FVector(0);
	UPROPERTY(Replicated)
	FName BoneNameToAttach = FName();
	//UPROPERTY(Replicated)
		TArray<UParticleSystemComponent*> ParticleEmitter;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	UParticleSystem* ParticleEffect = nullptr;
	//	UPROPERTY(Replicated)
	//TArray<UParticleSystem*> ParticleEffect;

};

UCLASS()
class TPS_NEXUS_API UTPS_StateEffect_ExecuteOnce : public UTPS_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName BoneNameHitted) override;

	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;

};

UCLASS()
class TPS_NEXUS_API UTPS_StateEffect_ExecuteTimer : public UTPS_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName BoneNameHitted) override;

	void DestroyObject() override;
	//virtual TArray<UParticleSystemComponent*> GetAllParticleEmmiters() override;

	virtual void Execute();
	//UFUNCTION(NetMulticast, Reliable)
	//	void FXSpawnByStateEffect_Multicast(AActor* Actor, FName BoneNameHitted);

	//UFUNCTION(NetMulticast, Reliable)
	//	void RemoveEffect_Multicast();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float RateTime = 1.0f;
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		bool ApplyRandomParticleEffect = true;*/

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;



	//UParticleSystemComponent* ParticleEmitter = nullptr;
	//probably remove

};
