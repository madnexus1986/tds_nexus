// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPS_NexusGameMode.generated.h"

UCLASS(minimalapi)
class ATPS_NexusGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPS_NexusGameMode();

	void PlayerCharacterDead();
};



