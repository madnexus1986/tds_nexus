// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPS_NexusGameMode.h"
#include "TPS_NexusPlayerController.h"
#include "TPS_Nexus/Character/TPS_NexusCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATPS_NexusGameMode::ATPS_NexusGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS_NexusPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	/*static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TPS/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}*/
}

void ATPS_NexusGameMode::PlayerCharacterDead()
{
}
