// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TPS_Nexus/FunctionLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TPS_Nexus/Weapons/WeaponDefault.h"
#include "TPSGameInstance_default.generated.h"

/**
 * 
 */
UCLASS()
class TPS_NEXUS_API UTPSGameInstance_default : public UGameInstance
{
	GENERATED_BODY()
	

public: 
	//table
	UPROPERTY(EditAnywhere, BlueprintreadWrite, Category = "WeaponSetting")
		UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ")
		UDataTable* DropItemInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo);
};
