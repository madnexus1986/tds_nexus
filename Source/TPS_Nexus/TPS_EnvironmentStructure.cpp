// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_EnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Engine/ActorChannel.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATPS_EnvironmentStructure::ATPS_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetReplicates(true);

}

// Called when the game starts or when spawned
void ATPS_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPS_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATPS_EnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UTPS_StateEffect*> ATPS_EnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATPS_EnvironmentStructure::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{
	//changes the value of applied effects to object and it's replicated on clients
	Effects.Remove(RemoveEffect);

	//calls function on server side for visual part
	RemoveStateEffects(RemoveEffect);
}

void ATPS_EnvironmentStructure::AddEffect(UTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}

FVector ATPS_EnvironmentStructure::GetEffectOffset()
{
	return EffectOffset;
}

FName ATPS_EnvironmentStructure::GetBoneNameToAttach()
{
	FName result = FName();
	USkeletalMeshComponent* mySkeletalMesh = Cast<USkeletalMeshComponent>(GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (mySkeletalMesh)
	{
		result = BoneNameForEffectApply;
	}
	return result;
}

UParticleSystemComponent* ATPS_EnvironmentStructure::GetParticleStateEffectComponent(UParticleSystem* ParticleEffectToSpawn, FName BoneNameToAttach)
{
	UParticleSystemComponent* result = nullptr;
	if (BoneNameToAttach == FName())
	{
		BoneNameToAttach = GetBoneNameToAttach();
	}
	USceneComponent* mySkeletalMesh = Cast<USceneComponent>(this->GetComponentByClass(USkeletalMeshComponent::StaticClass()));

	FVector Loc = FVector();
	if (ParticleEffectToSpawn)
	{
		if (mySkeletalMesh)
		{
			result = UGameplayStatics::SpawnEmitterAttached(ParticleEffectToSpawn, mySkeletalMesh, BoneNameToAttach, GetEffectOffset(), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			ParticleSystemEffects.Add(result);
		}
		else
		{
			Loc = this->GetRootComponent()->GetComponentLocation() + GetEffectOffset();
			result = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleEffectToSpawn, FTransform(FRotator::ZeroRotator, Loc, FVector(1.0f)), false);
			ParticleSystemEffects.Add(result);
		}
	}
	return result;
}

void ATPS_EnvironmentStructure::SetEffectRemoveForClient_Implementation(UTPS_StateEffect* RemoveEffectLink)
{
	//change on server side variable value and it has a notify function which is called after that to apply changes on client side
	if (!RemoveEffectLink->bIsAutoDestroyParticleEffect)
	{
		EffectRemove = RemoveEffectLink;
	}
}

void ATPS_EnvironmentStructure::SetEffectAddForClient_Implementation(UTPS_StateEffect* AddEffectLink)
{
	//change on server side variable value and it has a notify function which is called after that to apply changes on client side
	EffectAdd = AddEffectLink;
}

void ATPS_EnvironmentStructure::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		GetParticleStateEffectComponent(EffectAdd->ParticleEffect, EffectAdd->BoneNameToAttach);
	}
}

void ATPS_EnvironmentStructure::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		RemoveStateEffects(EffectRemove);
	}
}

void ATPS_EnvironmentStructure::RemoveStateEffects(UTPS_StateEffect* RemoveEffectsLink)
{
	int32 i = 0;
	bool bIsFind = false;
	if (ParticleSystemEffects.Num() > 0)
	{
		while (i < ParticleSystemEffects.Num() && !bIsFind)
		{
			if (ParticleSystemEffects[i]->Template && RemoveEffectsLink->ParticleEffect && RemoveEffectsLink->ParticleEffect == ParticleSystemEffects[i]->Template)
			{
				bIsFind = true;
				ParticleSystemEffects[i]->DeactivateSystem();
				ParticleSystemEffects[i]->DestroyComponent();
				ParticleSystemEffects.RemoveAt(i);
			}
			i++;
		}
	}
}

void ATPS_EnvironmentStructure::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATPS_EnvironmentStructure::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, GetEffectOffset(), GetBoneNameToAttach());
}


bool ATPS_EnvironmentStructure::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);


	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void ATPS_EnvironmentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPS_EnvironmentStructure, Effects);
	DOREPLIFETIME(ATPS_EnvironmentStructure, EffectAdd);
	DOREPLIFETIME(ATPS_EnvironmentStructure, EffectRemove);
}
