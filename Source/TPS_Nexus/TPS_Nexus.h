// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTPS_Nexus, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogTPS_Nexus_Net, Log, All);
