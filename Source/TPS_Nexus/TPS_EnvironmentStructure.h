// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/Channel.h"
#include "TPS_Nexus/Interface/TPS_IGameActor.h"
#include "TPS_Nexus/Weapons/TPS_StateEffect.h"
#include "TPS_EnvironmentStructure.generated.h"

UCLASS()
class TPS_NEXUS_API ATPS_EnvironmentStructure : public AActor, public ITPS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATPS_EnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	//Effect
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Replicated, Category = "StateEffectsSettings")
	TArray<UTPS_StateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		UTPS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		UTPS_StateEffect* EffectRemove = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffectsSettings")
		FVector EffectOffset = FVector(0);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffectsSettings")
		FName BoneNameForEffectApply = FName();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffectsSettings")
		TArray<UParticleSystemComponent*> ParticleSystemEffects;

	//Interface
	TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTPS_StateEffect* RemoveEffect) override;
	void AddEffect(UTPS_StateEffect* NewEffect) override;
	FVector GetEffectOffset() override;
	FName GetBoneNameToAttach() override;
	UParticleSystemComponent* GetParticleStateEffectComponent(UParticleSystem* ParticleEffectToSpawn, FName BoneNameToAttach) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SetEffectRemoveForClient(UTPS_StateEffect* RemoveEffectLink);
	void SetEffectRemoveForClient_Implementation(UTPS_StateEffect* RemoveEffectLink) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SetEffectAddForClient(UTPS_StateEffect* AddEffectLink);
	void SetEffectAddForClient_Implementation(UTPS_StateEffect* AddEffectLink) override;
	//End Unterface

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();
	UFUNCTION()
		void RemoveStateEffects(UTPS_StateEffect* RemoveEffectsLink);

	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);

	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);
};
