// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_Nexus/Character/TPS_EnemyCharacter.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Engine/ActorChannel.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATPS_EnemyCharacter::ATPS_EnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATPS_EnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPS_EnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATPS_EnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

TArray<UTPS_StateEffect*> ATPS_EnemyCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATPS_EnemyCharacter::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{
	//changes the value of applied effects to object and it's replicated on clients
	Effects.Remove(RemoveEffect);

	//calls function on server side for visual part
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		RemoveStateEffects(RemoveEffect);
	}
}

void ATPS_EnemyCharacter::AddEffect(UTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (NewEffect->bIsAutoDestroyParticleEffect && NewEffect->ParticleEffect)
	{
		ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
	}
}

FVector ATPS_EnemyCharacter::GetEffectOffset()
{
	return EffectOffset;
}

FName ATPS_EnemyCharacter::GetBoneNameToAttach()
{
	FName result = FName();
	USkeletalMeshComponent* mySkeletalMesh = Cast<USkeletalMeshComponent>(GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (mySkeletalMesh)
	{
		result = BoneNameForEffectApply;
	}
	return result;
}

UParticleSystemComponent* ATPS_EnemyCharacter::GetParticleStateEffectComponent(UParticleSystem* ParticleEffectToSpawn, FName BoneNameToAttach)
{
	UParticleSystemComponent* result = nullptr;
	if (BoneNameToAttach == FName())
	{
		BoneNameToAttach = GetBoneNameToAttach();
	}
	if (ParticleEffectToSpawn)
	{
		USceneComponent* myMesh = Cast<USceneComponent>(this->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if (myMesh)
		{
			result = UGameplayStatics::SpawnEmitterAttached(ParticleEffectToSpawn, myMesh, BoneNameToAttach, GetEffectOffset(), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			ParticleSystemEffects.Add(result);
		}
	}
	return result;
}

void ATPS_EnemyCharacter::SetEffectRemoveForClient_Implementation(UTPS_StateEffect* RemoveEffectLink)
{
	//change on server side variable value and it has a notify function which is called after that to apply changes on client side
	if (!RemoveEffectLink->bIsAutoDestroyParticleEffect)
	{
		EffectRemove = RemoveEffectLink;
	}
}

void ATPS_EnemyCharacter::SetEffectAddForClient_Implementation(UTPS_StateEffect* AddEffectLink)
{
	//change on server side variable value and it has a notify function which is called after that to apply changes on client side
	EffectAdd = AddEffectLink;
}

void ATPS_EnemyCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		GetParticleStateEffectComponent(EffectAdd->ParticleEffect, EffectAdd->BoneNameToAttach);
	}
}

void ATPS_EnemyCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		RemoveStateEffects(EffectRemove);
	}
}

void ATPS_EnemyCharacter::RemoveStateEffects(UTPS_StateEffect* RemoveEffectsLink)
{
	int32 i = 0;
	bool bIsFind = false;
	if (ParticleSystemEffects.Num() > 0)
	{
		while (i < ParticleSystemEffects.Num() && !bIsFind)
		{
			if (ParticleSystemEffects[i]->Template && RemoveEffectsLink->ParticleEffect && RemoveEffectsLink->ParticleEffect == ParticleSystemEffects[i]->Template)
			{
				bIsFind = true;
				ParticleSystemEffects[i]->DeactivateSystem();
				ParticleSystemEffects[i]->DestroyComponent();
				ParticleSystemEffects.RemoveAt(i);
			}
			i++;
		}
	}
}

void ATPS_EnemyCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATPS_EnemyCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, GetEffectOffset(), GetBoneNameToAttach());
}


bool ATPS_EnemyCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);


	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void ATPS_EnemyCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPS_EnemyCharacter, Effects);
	DOREPLIFETIME(ATPS_EnemyCharacter, EffectAdd);
	DOREPLIFETIME(ATPS_EnemyCharacter, EffectRemove);
}
