// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_HealthComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UTPS_HealthComponent::UTPS_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void UTPS_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	
}


// Called every frame
void UTPS_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

float UTPS_HealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTPS_HealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

bool UTPS_HealthComponent::GetIsAlreadyDead()
{
	return bIsDead;
}

void UTPS_HealthComponent::ChangeHealthValue_OnServer_Implementation(float ChangeValue)
{

		ChangeValue = ChangeValue * CoefDamage;

		Health += ChangeValue;

		//OnHealthChange.Broadcast(Health, ChangeValue);
		OnHealthChangeEvent_Multicast(Health, ChangeValue);

		if (Health > 100.0f)
		{
			Health = 100.0f;
		}
		else
		{
			if (Health <= 0.0f && !bIsDead)
			{
				//OnDead.Broadcast();
				DeadEvent_Multicast();
				bIsDead = true;
			}
		}
}

void UTPS_HealthComponent::OnHealthChangeEvent_Multicast_Implementation(float NewHealth, float value)
{
	OnHealthChange.Broadcast(NewHealth, value);
}

void UTPS_HealthComponent::DeadEvent_Multicast_Implementation()
{
	OnDead.Broadcast();
}

void UTPS_HealthComponent::DeadEvent_Implementation()
{
	//BP
}

void UTPS_HealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTPS_HealthComponent, Health);
	DOREPLIFETIME(UTPS_HealthComponent, bIsDead);

}

