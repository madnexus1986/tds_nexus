// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPS_NexusCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/SkeletalMeshComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Animation/AnimInstance.h"
#include "TPS_Nexus/Game/TPSGameInstance_default.h"
#include "TPS_Nexus/Weapons/Projectiles/ProjectileDefault.h"
#include "TPS_Nexus/TPS_Nexus.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"



ATPS_NexusCharacter::ATPS_NexusCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	//if (DecalMaterialAsset.Succeeded())
	//{
	//	CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	//}
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.

	InventoryComponent = CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UMyTPSCharacter_HealthComponent>(TEXT("CharHealthComponent"));

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ATPS_NexusCharacter::CharDead);
	}

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATPS_NexusCharacter::InitWeapon);
	}

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	//Network
	bReplicates = true;
}

void ATPS_NexusCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	/*if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}*/
	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC && myPC->IsLocalPlayerController())
		//if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATPS_NexusCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}
}

	//InitWeapon(InitWeaponName);



void ATPS_NexusCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPS_NexusCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPS_NexusCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("ChangeToSprint"), EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::InputSprintPressed);
	NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::InputWalkPressed);
	NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::InputAimPressed);
	NewInputComponent->BindAction(TEXT("ChangeToSprint"), EInputEvent::IE_Released, this, &ATPS_NexusCharacter::InputSprintReleased);
	NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Released, this, &ATPS_NexusCharacter::InputWalkReleased);
	NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Released, this, &ATPS_NexusCharacter::InputAimReleased);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATPS_NexusCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TrySwicthNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TrySwitchPreviosWeapon);
	
	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TryAbilityEnabled);

	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::DropCurrentWeapon);
	
	

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);


	//NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATPS_NexusCharacter::TKeyPressed<1>);
	//NewInputComponent->BindKey(HotKeys[2], EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TKeyPressed<2>);
	//NewInputComponent->BindKey(HotKeys[3], EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TKeyPressed<3>);
	//NewInputComponent->BindKey(HotKeys[4], EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TKeyPressed<4>);
	//NewInputComponent->BindKey(HotKeys[5], EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TKeyPressed<5>);
	//NewInputComponent->BindKey(HotKeys[6], EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TKeyPressed<6>);
	//NewInputComponent->BindKey(HotKeys[7], EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TKeyPressed<7>);
	//NewInputComponent->BindKey(HotKeys[8], EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TKeyPressed<8>);
	//NewInputComponent->BindKey(HotKeys[9], EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TKeyPressed<9>);
	//NewInputComponent->BindKey(HotKeys[0], EInputEvent::IE_Pressed, this, &ATPS_NexusCharacter::TKeyPressed<0>);

}

void ATPS_NexusCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATPS_NexusCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATPS_NexusCharacter::InputAttackPressed()
{
	if (CharHealthComponent && !CharHealthComponent->GetIsAlreadyDead())
	{
		AttackCharEvent(true);
	}

}

void ATPS_NexusCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATPS_NexusCharacter::InputWalkPressed()
{
	WalkEnabled = true;
	ChangeMovementState();
}

void ATPS_NexusCharacter::InputWalkReleased()
{
	WalkEnabled = false;
	ChangeMovementState();
}

void ATPS_NexusCharacter::InputSprintPressed()
{
	SprintRunEnabled = true;
	ChangeMovementState();
}

void ATPS_NexusCharacter::InputSprintReleased()
{
	SprintRunEnabled = false;
	ChangeMovementState();
}

void ATPS_NexusCharacter::InputAimPressed()
{
	AimEnabled = true;
	ChangeMovementState();
}

void ATPS_NexusCharacter::InputAimReleased()
{
	AimEnabled = false;
	ChangeMovementState();
}

void ATPS_NexusCharacter::MovementTick(float DeltaTime)
{
	if (CharHealthComponent && !CharHealthComponent->GetIsAlreadyDead())
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

			FString SEnum = UEnum::GetValueAsString(GetMovementState());
			UE_LOG(LogTPS_Nexus_Net, Warning, TEXT("Movement state - %s"), *SEnum);

			if (MovementState == EMovementState::SprintRun_State)
			{
				FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
				FRotator myRotator = myRotationVector.ToOrientationRotator();
				SetActorRotation((FQuat(myRotator)));

				SetActorRotationByYaw_OnServer(myRotator.Yaw);
			}
			else
			{
				APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				if (myController)
				{
					FHitResult ResultHit;
					//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
					myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

					float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
					SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
					SetActorRotationByYaw_OnServer(FindRotatorResultYaw);

					if (CurrentWeapon)
					{
						FVector Displacement = FVector(0);
						bool bIsReduceDispersion = false;
						switch (MovementState)
						{
						case EMovementState::Aim_State:
							Displacement = FVector(0.0f, 0.0f, 160.0f);
							//CurrentWeapon->ShouldReduceDispersion = true;
							bIsReduceDispersion = true;
							break;
						case EMovementState::AimWalk_State:
							//CurrentWeapon->ShouldReduceDispersion = true;
							bIsReduceDispersion = true;
							Displacement = FVector(0.0f, 0.0f, 160.0f);
							break;
						case EMovementState::Walk_State:
							Displacement = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EMovementState::Run_State:
							Displacement = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EMovementState::SprintRun_State:
							break;
						default:
							break;
						}

						//CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
						CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacement, bIsReduceDispersion);
					}
				}
			}

			//if (CurrentWeapon)
			//	if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
			//		CurrentWeapon->ShouldReduceDispersion = true;
			//	else
			//		CurrentWeapon->ShouldReduceDispersion = false;
		}
	}
	
}

void ATPS_NexusCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPS_NexusCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

//update different character params
void ATPS_NexusCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintRunSpeed;
		break;
	default:
		break;
	}
	
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATPS_NexusCharacter::ChangeMovementState()
{
	EMovementState NewState = EMovementState::Run_State;
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		NewState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			NewState = EMovementState::SprintRun_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && AimEnabled)
			{
				NewState = EMovementState::AimWalk_State;
			}
			else
			{
				if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
				{
					NewState = EMovementState::Walk_State;
				}
				else
				{
					if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
					{
						NewState = EMovementState::Aim_State;
					}
				}
			}
		}
		
	}

	SetMovementState_OnServer(NewState);
	//CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon_OnServer(NewState);
	}
}

AWeaponDefault* ATPS_NexusCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATPS_NexusCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	//Runs only on Server
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	
	UTPSGameInstance_default* myGI = Cast<UTPSGameInstance_default>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					
					myWeapon->WeaponSetting = myWeaponInfo;
					//myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;
					//Remove debug
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon_OnServer(MovementState);
					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					CurrentIndexWeapon = NewCurrentIndexWeapon;
					
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPS_NexusCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPS_NexusCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATPS_NexusCharacter::WeaponFireStart);

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if (InventoryComponent)
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPS_NexusCharacter::InitWeapon - Weapon not found in table - NULL"));
		}
	}
	

	
}

//if character is not dead and has weapon and it's not reloading at that moment- call funcion to try reload his weapon
void ATPS_NexusCharacter::TryReloadWeapon()
{
	if (CharHealthComponent && !CharHealthComponent->GetIsAlreadyDead() && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		TryReloadWeapon_OnServer();
	}
}

//calls event in blueprint to play animation for reloading
void ATPS_NexusCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

//updates all info after reloading - decrease ammo used, update ammo loaded in weapon
void ATPS_NexusCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

//switches weapons to chosen index if there are any possible to switch
void ATPS_NexusCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();
			}

			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
}

//if inventorycomponent exists calls function to drop current weapon 
void ATPS_NexusCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		//FDropItem ItemInfo;
		InventoryComponent->DropWeaponByIndex_OnServer(CurrentIndexWeapon);
	}
}

void ATPS_NexusCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	WeaponFireStart_BP(Anim);
}

void ATPS_NexusCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATPS_NexusCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	//in BP
}

void ATPS_NexusCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATPS_NexusCharacter::RemoveCurrentWeapon()
{
}

UDecalComponent* ATPS_NexusCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

EMovementState ATPS_NexusCharacter::GetMovementState()
{
	return MovementState;
}

TArray<UTPS_StateEffect*> ATPS_NexusCharacter::GetCurrentEffectsOnChar()
{
	return Effects;
}

int32 ATPS_NexusCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

bool ATPS_NexusCharacter::GetAimState()
{
	
	return AimEnabled;
}

void ATPS_NexusCharacter::CharDead_BP_Implementation()
{
	//BP
}

void ATPS_NexusCharacter::TrySwicthNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}

void ATPS_NexusCharacter::TrySwitchPreviosWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

void ATPS_NexusCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)
	{
		UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, GetBoneNameToAttach());
		}
	}
}

FVector ATPS_NexusCharacter::GetEffectOffset()
{
	return AbilityApplyOffset;
}

FName ATPS_NexusCharacter::GetBoneNameToAttach()
{
	return BoneNameForStateEffectsApply;
}



void ATPS_NexusCharacter::CharDead()
{
	CharDead_BP();
	if (HasAuthority())
	{
		float TimeAnim = 0.0f;
		int8 rnd = FMath::RandHelper(DeadsAnim.Num());
		if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			TimeAnim = DeadsAnim[rnd]->GetPlayLength();
			//GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
			PlayAnim_Multicast(DeadsAnim[rnd]);
		}

		if (GetController())
		{
			GetController()->UnPossess();
		}

		//UnPossessed();

		//timer ragdoll
		GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATPS_NexusCharacter::EnableRagDoll_Multicast, TimeAnim, false);
		SetLifeSpan(20.0f);
		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->SetLifeSpan(20.0f);
		}
	}
	else
	{
		if (GetCursorToWorld())
		{
			GetCursorToWorld()->SetVisibility(false);
		}

		//UE_LOG(LogTemp, Warning, TEXT("ATPS_NexusCharacter::CharDead - DIED"));

		AttackCharEvent(false);
	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}

	
}

float ATPS_NexusCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (CharHealthComponent && !CharHealthComponent->GetIsAlreadyDead())
	{
		CharHealthComponent->ChangeHealthValue_OnServer(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, BoneNameForStateEffectsApply, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
		}
	}
	return ActualDamage;
}

void ATPS_NexusCharacter::EnableRagDoll_Multicast_Implementation()
{

	if (GetMesh())
	{
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

bool ATPS_NexusCharacter::GetisAlive()
{
	bool result = false;
	if (CharHealthComponent)
	{
		result = !CharHealthComponent->GetIsAlreadyDead();
	}
	return result;
}

void ATPS_NexusCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ATPS_NexusCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	//if (Controller && !Controller->IsLocalPlayerController())
	//{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	//}
}

void ATPS_NexusCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void ATPS_NexusCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();

}

EPhysicalSurface ATPS_NexusCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	

	return Result;
}

TArray<UTPS_StateEffect*> ATPS_NexusCharacter::GetAllCurrentEffects()
{
	return Effects;
}

//Is called when StateEffect makes a call to interface function. Actually it is called only on server side
void ATPS_NexusCharacter::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{
	//changes the value of applied effects to object and it's replicated on clients
	Effects.Remove(RemoveEffect);

	//calls function on server side for visual part
	//SwitchEffect(RemoveEffect, false);
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		RemoveStateEffects(RemoveEffect);
	}


	//EffectRemove = RemoveEffect;
}

//Is called when StateEffect makes a call to interface function on initobject
void ATPS_NexusCharacter::AddEffect(UTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (NewEffect->bIsAutoDestroyParticleEffect && NewEffect->ParticleEffect)
	{
		ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
	}

	//SwitchEffect(NewEffect, true);
	//EffectAdd = NewEffect;
}

UParticleSystemComponent* ATPS_NexusCharacter::GetParticleStateEffectComponent(UParticleSystem* ParticleEffectToSpawn, FName BoneNameToAttach)
{
	UParticleSystemComponent* result = nullptr;
	if (BoneNameToAttach == FName())
	{
		BoneNameToAttach = GetBoneNameToAttach();
	}
	if (ParticleEffectToSpawn)
	{
		USceneComponent* myMesh = Cast<USceneComponent>(this->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if (myMesh)
		{
			result = UGameplayStatics::SpawnEmitterAttached(ParticleEffectToSpawn, myMesh, BoneNameToAttach, GetEffectOffset(), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			ParticleSystemEffects.Add(result);
		}
	}

	
	return result;
}

void ATPS_NexusCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATPS_NexusCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, GetEffectOffset(), GetBoneNameToAttach());
}

void ATPS_NexusCharacter::SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd)
{

}



void ATPS_NexusCharacter::SetEffectRemoveForClient_Implementation(UTPS_StateEffect* RemoveEffectLink)
{
	//change on server side variable value and it has a notify function which is called after that to apply changes on client side
	if (!RemoveEffectLink->bIsAutoDestroyParticleEffect)
	{
		EffectRemove = RemoveEffectLink;
	}
}

void ATPS_NexusCharacter::SetEffectAddForClient_Implementation(UTPS_StateEffect* AddEffectLink)
{
	//change on server side variable value and it has a notify function which is called after that to apply changes on client side
	EffectAdd = AddEffectLink;
}

//function is called when EffectAdd variable changes its value on client
void ATPS_NexusCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		//TArray<UParticleSystemComponent*> StateEffectEmmiters = EffectAdd->ParticleEmitter;
		//for (int8 i = 0; i < StateEffectEmmiters.Num(); i++)
		//{
		//	GetParticleStateEffectComponent(StateEffectEmmiters[i]->Template, EffectAdd->BoneNameToAttach);
		//}
		GetParticleStateEffectComponent(EffectAdd->ParticleEffect, EffectAdd->BoneNameToAttach);
		//SwitchEffect(EffectAdd, true);
	}

}

//function is called when EffectRemove variable changes its value on client
void ATPS_NexusCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		RemoveStateEffects(EffectRemove);
			//SwitchEffect(EffectRemove, false);
	}
}

void ATPS_NexusCharacter::RemoveStateEffects(UTPS_StateEffect* RemoveEffectsLink)
{
	//int32 i = 0;
	//bool bIsFind = false;
	//TArray<UParticleSystemComponent*> StateEffectEmmiters = RemoveEffectsLink->ParticleEmitter;
	//for (int8 i = 0; i < ParticleSystemEffects.Num(); i++)
	//{
	//	for (int8 b = 0; b < StateEffectEmmiters.Num(); b++)
	//	{
	//		if (ParticleSystemEffects[i]->Template && StateEffectEmmiters[b] && StateEffectEmmiters[b]->Template == ParticleSystemEffects[i]->Template)
	//		{
	//			bIsFind = true;
	//			ParticleSystemEffects[i]->DeactivateSystem();
	//			ParticleSystemEffects[i]->DestroyComponent();
	//			ParticleSystemEffects.RemoveAt(i);
	//		}
	//	}

	//}
		int32 i = 0;
		bool bIsFind = false;
		if (ParticleSystemEffects.Num() > 0)
		{
			while (i < ParticleSystemEffects.Num() && !bIsFind)
			{
				if (ParticleSystemEffects[i]->Template && RemoveEffectsLink->ParticleEffect && RemoveEffectsLink->ParticleEffect == ParticleSystemEffects[i]->Template)
				{
					bIsFind = true;
					ParticleSystemEffects[i]->DeactivateSystem();
					ParticleSystemEffects[i]->DestroyComponent();
					ParticleSystemEffects.RemoveAt(i);
				}
				i++;
			}
		}
	
}


//initializes reload on server side(because server has all neseccery info) if ammo is enough and weapon can be reloaded
void ATPS_NexusCharacter::TryReloadWeapon_OnServer_Implementation()
{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
			CurrentWeapon->InitReload();
}

void ATPS_NexusCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Anim)
{
	if (GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
	}

}

bool ATPS_NexusCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);


	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void ATPS_NexusCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPS_NexusCharacter, MovementState);
	DOREPLIFETIME(ATPS_NexusCharacter, CurrentWeapon);
	DOREPLIFETIME(ATPS_NexusCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(ATPS_NexusCharacter, Effects);
	DOREPLIFETIME(ATPS_NexusCharacter, EffectAdd);
	DOREPLIFETIME(ATPS_NexusCharacter, EffectRemove);
}
