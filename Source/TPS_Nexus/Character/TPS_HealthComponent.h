// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TPS_HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, ChangeValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);
USTRUCT(BlueprintType)
struct FStatsParam
{
	GENERATED_BODY()
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_NEXUS_API UTPS_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTPS_HealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Health")
		FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Health")
		FOnDead OnDead;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(Replicated)
	float Health = 100.0f;
	UPROPERTY(Replicated)
	bool bIsDead = false;

public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CoefDamage = 1.0f;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable, Category = "Health")
		float GetCurrentHealth();
	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetCurrentHealth(float NewHealth);
	UFUNCTION(BlueprintCallable, Category = "Health")
		bool GetIsAlreadyDead();
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
		virtual void ChangeHealthValue_OnServer(float ChangeValue);
	UFUNCTION(BlueprintNativeEvent)
		void DeadEvent();

	UFUNCTION(NetMulticast, Reliable)
		void OnHealthChangeEvent_Multicast(float NewHealth, float value);
	UFUNCTION(NetMulticast, Reliable)
		void DeadEvent_Multicast();

		
};
