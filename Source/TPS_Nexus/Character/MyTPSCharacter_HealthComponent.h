// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS_Nexus/Character/TPS_HealthComponent.h"
#include "MyTPSCharacter_HealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class TPS_NEXUS_API UMyTPSCharacter_HealthComponent : public UTPS_HealthComponent
{
	GENERATED_BODY()

public:
	FTimerHandle TimerHandle_CoolDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;

protected:
	float Shield = 100.0f;

public:



	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverRate = 0.1f;

	void ChangeHealthValue_OnServer(float ChangeValue) override;

	UFUNCTION(BlueprintCallable)
	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);

	void CoolDownShieldEnd();

	void RecoveryShield();

	UFUNCTION(NetMulticast, Reliable)
		void ShieldChangeEvent_Multicast(float NewShield, float Damage);
	
};
