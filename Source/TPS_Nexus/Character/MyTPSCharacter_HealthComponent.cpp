// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTPSCharacter_HealthComponent.h"

void UMyTPSCharacter_HealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	
	float CurrentDamage = ChangeValue * CoefDamage;


	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//fx
		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}
	
	//for character
}

//return current shield value
float UMyTPSCharacter_HealthComponent::GetCurrentShield()
{
	return Shield;
}

void UMyTPSCharacter_HealthComponent::ChangeShieldValue(float ChangeValue)
{

	Shield += ChangeValue;



	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShieldTimer, this, &UMyTPSCharacter_HealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);
		UE_LOG(LogTemp, Warning, TEXT("Cooldownshiledtimer started to count cooldown"));

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		UE_LOG(LogTemp, Warning, TEXT("shieldrecoverrate timer cleared "));
	}
	//OnShieldChange.Broadcast(Shield, ChangeValue);
	ShieldChangeEvent_Multicast(Shield, ChangeValue);
}

void UMyTPSCharacter_HealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UMyTPSCharacter_HealthComponent::RecoveryShield, ShieldRecoverRate, true);
		UE_LOG(LogTemp, Warning, TEXT("shieldrecoverrate timer started because cooldown ended"));
	}


}

void UMyTPSCharacter_HealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
			UE_LOG(LogTemp, Warning, TEXT("shieldrecoverrate timer cleared because shield reached 100"));
		}
	}
	else
	{
		Shield = tmp;
	}
	//OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
	ShieldChangeEvent_Multicast(Shield, ShieldRecoverValue);
}

void UMyTPSCharacter_HealthComponent::ShieldChangeEvent_Multicast_Implementation(float NewShield, float Damage)
{
	OnShieldChange.Broadcast(NewShield, Damage);
}
