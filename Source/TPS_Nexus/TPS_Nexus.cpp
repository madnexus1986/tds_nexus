// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPS_Nexus.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPS_Nexus, "TPS_Nexus" );

DEFINE_LOG_CATEGORY(LogTPS_Nexus)
DEFINE_LOG_CATEGORY(LogTPS_Nexus_Net)
 