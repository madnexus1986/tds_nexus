// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TPS_Nexus/Weapons/TPS_StateEffect.h"
#include "TPS_Nexus/FunctionLibrary/Types.h"
#include "TPS_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTPS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TPS_NEXUS_API ITPS_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	//UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Event")
	//	bool AvailableForEffectsBP();

	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Event")
	//	bool AvailableForEffects();

	//	virtual bool AvailableForEffectsOnlyCPP();

	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTPS_StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTPS_StateEffect* RemoveEffect);
	virtual void AddEffect(UTPS_StateEffect* NewEffect);
	virtual FVector GetEffectOffset();
	virtual FName GetBoneNameToAttach();
	virtual UParticleSystemComponent* GetParticleStateEffectComponent(UParticleSystem* ParticleEffectToSpawn, FName BoneNameToAttach);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetEffectAddForClient(UTPS_StateEffect* AddEffectLink);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetEffectRemoveForClient(UTPS_StateEffect* RemoveEffectLink);

	//inventory
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);
};
